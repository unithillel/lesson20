<?php
use Carbon\Carbon;

require_once 'vendor/autoload.php';

$carbonNow = Carbon::now();
$carbonYesterday = Carbon::create(2019,11,20);
$carbonMonthAgo = Carbon::create(2019,10,20);

echo $carbonNow->format('Y.m.d');
$carbonYesterday->addDay();
echo '<br>';
echo $carbonYesterday->format('Y.m.d');
echo '<br>';
echo $carbonNow->diffForHumans($carbonYesterday);
echo '<br>';
echo $carbonNow->diffInDays($carbonMonthAgo);